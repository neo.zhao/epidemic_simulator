package backend;

import java.util.ArrayList;

import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;

public class Simulation {
	
	int step_size;
	int sim_speed;
	int day;
	Sim_Stage stage;
	
	public Simulation(int spSz, int smSd, Disease d, Society s) {
		this.step_size = spSz;
		this.sim_speed =smSd;
		day = 0;
		this.stage = new Sim_Stage(d, s);
	}
	
	/* GETTERS AND SETTERS */
	
	public int getStep_size() {return step_size;}
	public int getSim_speed() {return sim_speed;}
	public Sim_Stage getStage() {return stage;}
	
	public void setStep_size(int step_size) {this.step_size = step_size;}
	public void setSim_speed(int sim_speed) {this.sim_speed = sim_speed;}
	
	/* OTHER METHODS */
	
	public void reset(Disease d, Society s) {
		stage = new Sim_Stage(d, s);
		day = 0;
	}
	
	public void modify(Disease d, Society s) {
		stage.modify(d, s);
	}

	public void step() {
		for (int i = 0; i < step_size; i++) {
			stage.tick();
			day++;
		}
	}
	
	public ArrayList<Series> getScatterData() {
		
		Agent[][] grid = stage.getAgent_grid();

		XYChart.Series untouched = new XYChart.Series<Integer, Integer>();
		untouched.setName("Untouched");
		XYChart.Series infected = new XYChart.Series<Integer, Integer>();
		infected.setName("Infected");
		XYChart.Series dead = new XYChart.Series<Integer, Integer>();
		dead.setName("Dead");
		XYChart.Series recovered = new XYChart.Series<Integer, Integer>();
		recovered.setName("Recovered");
		
		for (Agent[] row : grid) {
			for (Agent a: row) {
				switch(a.getdStatus()) {
				case UNTOUCHED:
					untouched.getData().add(new XYChart.Data<Integer, Integer>(a.getPosX(), a.getPosY()));
					break;
				case INFECTED:
					infected.getData().add(new XYChart.Data<Integer, Integer>(a.getPosX(), a.getPosY()));
					break;
				case DEAD:
					dead.getData().add(new XYChart.Data<Integer, Integer>(a.getPosX(), a.getPosY()));
					break;
				case IMMUNE:
					recovered.getData().add(new XYChart.Data<Integer, Integer>(a.getPosX(), a.getPosY()));
					break;
				}
			}
		}
		
		ArrayList<Series> data = new ArrayList<Series>();
		data.add(dead);
		data.add(infected);
		data.add(untouched);
		data.add(recovered);
		
		return data;
	}

	public ArrayList<Integer> getAreaData() {
		ArrayList<Integer> data = new ArrayList<Integer>();
		data.add(day);
		data.add(stage.count_agents_with_disease_status(Agent_Disease_Status.DEAD));
		data.add(stage.count_agents_with_disease_status(Agent_Disease_Status.INFECTED));
		data.add(stage.count_agents_with_disease_status(Agent_Disease_Status.UNTOUCHED));
		data.add(stage.count_agents_with_disease_status(Agent_Disease_Status.IMMUNE));
		return data;
	}
}
