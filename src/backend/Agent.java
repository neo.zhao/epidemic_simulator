package backend;

public class Agent {
	
	private Agent_Disease_Status dStatus;
	private Agent_Society_Status sStatus;
	private int posX;
	private int posY;
	private int days_sick;
	
	public Agent(Agent_Disease_Status sD, Agent_Society_Status sS, int x, int y) {
		this.dStatus = sD;
		this.sStatus = sS;
		this.posX = x;
		this.posY = y;
		this.days_sick = 0;
	}
	
	public Agent(int x, int y) {this(Agent_Disease_Status.UNTOUCHED, Agent_Society_Status.NORMAL, x, y);}
	
	/* GETTERS AND SETTERS */
	
	public Agent_Disease_Status getdStatus() {return dStatus;}
	public Agent_Society_Status getsStatus() {return sStatus;}
	public int getPosX() {return posX;}
	public int getPosY() {return posY;}
	public int getDays_sick() {return days_sick;}
	
	public void setdStatus(Agent_Disease_Status dStatus) {this.dStatus = dStatus;}
	public void setsStatus(Agent_Society_Status sStatus) {this.sStatus = sStatus;}
	
	/* OTHER METHODS */
	
	public void incrementDaysSick() {days_sick++;}
}
