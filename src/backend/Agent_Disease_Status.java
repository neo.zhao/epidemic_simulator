package backend;

public enum Agent_Disease_Status {
	UNTOUCHED,
	INFECTED,
	IMMUNE,
	DEAD
}
