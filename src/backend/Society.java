package backend;

public class Society {
	
	private int population;
	private int default_distance;
	private int social_distancing_bonus;
	private double social_distancing_percentage;
	private int hospital_capacity;
	private double hospital_mortality_percentage_reduction;
	
	public Society(int p, int dD, int sDB, double sDP, int hC, double hMPR) {
		this.population = p;
		this.default_distance = dD;
		this.social_distancing_bonus = sDB;
		this.social_distancing_percentage = sDP;
		this.hospital_capacity = hC;
		this.hospital_mortality_percentage_reduction = hMPR;
	}
	
	/* GETTERS AND SETTERS */
	
	public int getPopulation() {return population;}
	public int getDefault_distance() {return default_distance;}
	public int getSocial_distancing_bonus() {return social_distancing_bonus;}
	public double getSocial_distancing_percentage() {return social_distancing_percentage;}
	public int getHospital_capacity() {return hospital_capacity;}
	public double getHospital_mortality_percentage_reduction() {return hospital_mortality_percentage_reduction;}
	
	/* OTHER METHODS */
	
}
