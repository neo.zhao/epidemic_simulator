package backend;

public class Disease {
	
	private int infection_range;
	private int infection_period;
	private double infection_chance;
	private double mortality_rate;
	
	public Disease(int iR, int iP, double iC, double m) {
		this.infection_range = iR;
		this.infection_period = iP;
		this.infection_chance =iC;
		this.mortality_rate = m;
	}
	
	/* GETTERS AND SETTERS */
	
	public int getInfection_range() {return infection_range;}
	public int getInfection_period() {return infection_period;}
	public double getInfection_chance() {return infection_chance;}
	public double getMortality_rate() {return mortality_rate;}
	
	/* OTHER METHODS */
}
