package backend;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

public class Sim_Stage {
	
	private int size;
	private int population;
	private Agent[][] agent_grid;
	private Disease disease;
	private Society society;
	
	public Sim_Stage(Disease d, Society s) {
		
		this.population = s.getPopulation();
		this.disease = d;
		this.society = s;
		
		// calculating size based on pops
		int count = 1;
		while (count*count < s.getPopulation()) {
			count++;
		}
		this.size = count;
		
		Random generator = new Random();
		
		// fill agent_grid with untouched, normal agents that have society default distance
		agent_grid = new Agent[size][size];
		for (int x = 0; x < size; x++) {
			for (int y = 0; y < size; y++) {
				agent_grid[x][y] = new Agent( x, y);
			}
		}
		
		// start infection with a random individual
		int iX = generator.nextInt(size);
		int iY = generator.nextInt(size);
		agent_grid[iX][iY].setdStatus(Agent_Disease_Status.INFECTED);
		
		// apply social distancing as society outlines
		while ((double)count_agents_with_society_status(Agent_Society_Status.SOCIAL_DISTANCING) /
				(double)population < society.getSocial_distancing_percentage()) {
			
			// select random individuals to be social distancers
			int sdX = generator.nextInt(size);
			int sdY = generator.nextInt(size);
			
			// check if selected individual is already social distancing
			if (agent_grid[sdX][sdY].getsStatus() != Agent_Society_Status.SOCIAL_DISTANCING) {
				agent_grid[sdX][sdY].setsStatus(Agent_Society_Status.SOCIAL_DISTANCING);
			}
		}
	}
	
	/* GETTERS AND SETTERS */
	
	public Agent[][] getAgent_grid() {return agent_grid;}
	
	/* OTHER METHODS */
	
	/**
	 * This method counts the number of agents in the stage with the given disease status
	 * @param a the agent disease status that will be counted
	 * @return the number of agents in the stage with this disease status
	 */
	public int count_agents_with_disease_status(Agent_Disease_Status a) {
		
		int count = 0;
		
		for (int x = 0; x < size; x++) {
			for (int y = 0; y < size; y++) {
				if (agent_grid[x][y].getdStatus() == a) {
					count++;
				}
			}
		}
		
		return count;
	}
	
	/**
	 * This method counts the number of agents in the stage with the given society status
	 * @param a the agent society status that will be counted
	 * @return the number of agents in the stage with this society status
	*/
	public int count_agents_with_society_status(Agent_Society_Status a) {
		
		int count = 0;
		
		for (int x = 0; x < size; x++) {
			for (int y = 0; y < size; y++) {
				if (agent_grid[x][y].getsStatus() == a) {
					count++;
				}
			}
		}
		
		return count;
	}
	
	/**
	 * This method returns a list of the agents in the stage which have the given disease status
	 * @param a the disease status to be targeted
	 * @return an ArrayList of all agents in the stage with the targeted disease status
	 */
	public ArrayList<Agent> gather_agents_with_disease_status(Agent_Disease_Status a) {
		
		ArrayList<Agent> gathered = new ArrayList<Agent>();
		
		for (int x = 0; x < size; x++) {
			for (int y = 0; y < size; y++) {
				if (agent_grid[x][y].getdStatus() == a) {
					gathered.add(agent_grid[x][y]);
				}
			}
		}
		
		return gathered;
	}

	/**
	 * This method returns a list of the agents in the stage which have the given society status
	 * @param a the society status to be targeted
	 * @return an ArrayList of all agents in the stage with the targeted society status
	 */
	public ArrayList<Agent> gather_agents_with_society_status(Agent_Society_Status a) {
		
		ArrayList<Agent> gathered = new ArrayList<Agent>();
		
		for (int x = 0; x < size; x++) {
			for (int y = 0; y < size; y++) {
				if (agent_grid[x][y].getsStatus() == a) {
					gathered.add(agent_grid[x][y]);
				}
			}
		}
		
		return gathered;
	}
	
	/**
	 * This method recursively gathers all agents within range of a given agent
	 * @param a the starting agent
	 * @param range the range to be checked
	 * @return a HashSet of agents within range of the starting agent
	 */
	public HashSet<Agent> gather_in_range(Agent a, int range){
		
		HashSet<Agent> in_range = new HashSet<Agent>();
		in_range.add(a);
		
		// check above
		if (a.getPosY() < size - 1) {
			Agent above = agent_grid[a.getPosX()][a.getPosY() + 1];
			int distance = 2*society.getDefault_distance();
			if (a.getsStatus() == Agent_Society_Status.SOCIAL_DISTANCING) {
				distance += society.getSocial_distancing_bonus();
			}
			if (above.getsStatus() == Agent_Society_Status.SOCIAL_DISTANCING) {
				distance += society.getSocial_distancing_bonus();
			}
			if (distance <= range) {
				in_range.addAll(gather_in_range(above, range - distance));
			}
		}
		
		// check right
		if (a.getPosX() < size - 1) {
			Agent right = agent_grid[a.getPosX() + 1][a.getPosY()];
			int distance = 2*society.getDefault_distance();
			if (a.getsStatus() == Agent_Society_Status.SOCIAL_DISTANCING) {
				distance += society.getSocial_distancing_bonus();
			}
			if (right.getsStatus() == Agent_Society_Status.SOCIAL_DISTANCING) {
				distance += society.getSocial_distancing_bonus();
			}
			if (distance <= range) {
				in_range.addAll(gather_in_range(right, range - distance));
			}
		}
		
		// check below
		if (a.getPosY() > 0) {
			Agent below = agent_grid[a.getPosX()][a.getPosY() - 1];
			int distance = 2*society.getDefault_distance();
			if (a.getsStatus() == Agent_Society_Status.SOCIAL_DISTANCING) {
				distance += society.getSocial_distancing_bonus();
			}
			if (below.getsStatus() == Agent_Society_Status.SOCIAL_DISTANCING) {
				distance += society.getSocial_distancing_bonus();
			}
			if (distance <= range) {
				in_range.addAll(gather_in_range(below, range - distance));
			}
		}
		
		// check left
		if (a.getPosX() > 0) {
			Agent left = agent_grid[a.getPosX() - 1][a.getPosY()];
			int distance = 2*society.getDefault_distance();
			if (a.getsStatus() == Agent_Society_Status.SOCIAL_DISTANCING) {
				distance += society.getSocial_distancing_bonus();
			}
			if (left.getsStatus() == Agent_Society_Status.SOCIAL_DISTANCING) {
				distance += society.getSocial_distancing_bonus();
			}
			if (distance <= range) {
				in_range.addAll(gather_in_range(left, range - distance));
			}
		}
		
		return in_range;
	}
	
	/**
	 * This method advances the state of the simulation by a single day
	 */
	public void tick() {
		
		ArrayList<Agent> old_infected = gather_agents_with_disease_status(Agent_Disease_Status.INFECTED);
		
		// update stuff for already infected
		for (int i = 0; i < old_infected.size(); i++) {
			
			// update days sick
			old_infected.get(i).incrementDaysSick();
			
			// check and update status if infection period is over
			if (old_infected.get(i).getDays_sick() == disease.getInfection_period()) {
				
				// check if agent is in hospital
				if (i < society.getHospital_capacity()) {
					if (Math.random() < disease.getMortality_rate() - society.getHospital_mortality_percentage_reduction()) {
						old_infected.get(i).setdStatus(Agent_Disease_Status.DEAD);
					} else {
						old_infected.get(i).setdStatus(Agent_Disease_Status.IMMUNE);
					}
				// not in hospital
				} else {
					if (Math.random() < disease.getMortality_rate()) {
						old_infected.get(i).setdStatus(Agent_Disease_Status.DEAD);
					} else {
						old_infected.get(i).setdStatus(Agent_Disease_Status.IMMUNE);
					}
				}
			}
		}
		
		// add new infected
		for (Agent a : old_infected) {
			
			for (Agent i : gather_in_range(a, disease.getInfection_range())) {
				
				if (i.getdStatus() == Agent_Disease_Status.UNTOUCHED && Math.random() < disease.getInfection_chance()) {
					i.setdStatus(Agent_Disease_Status.INFECTED);
				}
			}
		}
	}

	public void modify(Disease d, Society s) {
		disease = d;
		society = s;
		Random generator = new Random();
		
		// calculating size based on new pops
		int count = 1;
		while (count*count < s.getPopulation()) {
			count++;
		}
		this.size = count;
		
		// fill new_grid with untouched, normal agents that have society default distance
		Agent[][] new_grid = new Agent[size][size];
		for (int x = 0; x < size; x++) {
			for (int y = 0; y < size; y++) {
				new_grid[x][y] = new Agent(x, y);
			}
		}
		//place old agents into the new_grid
		for (int x = 0; x < agent_grid.length && x < size; x++) {
			for (int y = 0; y < agent_grid[x].length && x < size; y++) {
				new_grid[x][y] = agent_grid[x][y];
			}
		}
		agent_grid = new_grid;
		
		
		// update social distancing members in society
		while ((double)count_agents_with_society_status(Agent_Society_Status.SOCIAL_DISTANCING) /
				(double)population < society.getSocial_distancing_percentage()) {
			
			// select random individuals to be social distancers
			int sdX = generator.nextInt(size);
			int sdY = generator.nextInt(size);
			
			// check if selected individual is already social distancing
			if (agent_grid[sdX][sdY].getsStatus() != Agent_Society_Status.SOCIAL_DISTANCING) {
				agent_grid[sdX][sdY].setsStatus(Agent_Society_Status.SOCIAL_DISTANCING);
			}
		}
		while ((double)count_agents_with_society_status(Agent_Society_Status.SOCIAL_DISTANCING) /
				(double)population > society.getSocial_distancing_percentage()) {
			gather_agents_with_society_status(Agent_Society_Status.SOCIAL_DISTANCING).get(0).setsStatus(Agent_Society_Status.NORMAL);
		}
	}
	
}
