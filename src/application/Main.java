package application;
	
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import backend.Agent_Disease_Status;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.fxml.FXMLLoader;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) throws IOException {
		
		// main set-up
		FXMLLoader loader = new FXMLLoader( getClass().getResource("Epidemic Simulator.fxml") );
		SplitPane root = loader.load();
		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.setMaximized(true);
		primaryStage.show();
		
		SimulationController rc = loader.getController();
		
		
		Timer timer = new Timer();
		TimerTask t = new TimerTask() {
			public void run() {
				Platform.runLater(() -> {
					if (rc.epidemic_sim.getStage().count_agents_with_disease_status(Agent_Disease_Status.INFECTED) == 0) {
						rc.playButton.setText("Play");
						rc.playing = false;
					} else if (rc.playing) {
						rc.stepButton.fireEvent(new ActionEvent());
					}
				});
			}
		};
		timer.scheduleAtFixedRate(t, 0, 1000l);
	}
	
	// Will was here
	public static void main(String[] args) {
		launch(args);
	}
}
