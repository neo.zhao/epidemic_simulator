package application;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import backend.Disease;
import backend.Simulation;
import backend.Society;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.StackedAreaChart;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

public class SimulationController {
	
	Disease d = new Disease(0, 0, 0, 0);
	Society s = new Society(0, 0, 0, 0, 0, 0);
	public Simulation epidemic_sim = new Simulation(1, 1, d, s);
	public static boolean playing = false;
	static DecimalFormat df = new DecimalFormat();
	
	static {
		df.setMinimumFractionDigits(2);
		df.setMaximumFractionDigits(2);
	}
	
	/* FX IDs */
	
	//Simulation Controls
	@FXML public Button playButton;
	@FXML public Button stepButton;
	
	//Basics
	@FXML private TextField stepSizeTextField;
	@FXML private TextField simSpeedTextField;
	
	//Disease
	@FXML private TextField infectionRangeTextField;
	@FXML private TextField infectiousPeriodTextField;
	@FXML private Slider infectionRateSlider;
	@FXML private Label infectionRateDisplayLabel;
	@FXML private Slider mortalityRateSlider;
	@FXML private Label mortalityRateDisplayLabel;
	
	//Society
	@FXML private TextField populationTextField;
	@FXML private TextField defaultDistanceTextField;
	@FXML private TextField hospitalCapacityTextField;
	@FXML private Slider mortalityReductionRateSlider;
	@FXML private Label mortalityReductionRateDisplayLabel;
	@FXML private TextField bonusDistanceTextField;
	@FXML private Slider percentPopulationSlider;
	@FXML private Label percentPopulationDisplayLabel;
	
	//Display
	@FXML private ScatterChart<NumberAxis, NumberAxis> simGrid;
	@FXML private StackedAreaChart<NumberAxis, NumberAxis> sirdCurve;
	@FXML private Label daysDisplayLabel;
	@FXML private Label untouchedDisplayLabel;
	@FXML private Label infectedDisplayLabel;
	@FXML private Label deadDisplayLabel;
	@FXML private Label recoveredDisplayLabel;
	@FXML private StackedBarChart<NumberAxis, CategoryAxis> hidBar;
	@FXML private PieChart pie;
	 
	/* Event Handlers */
	
	// Simulation Controls	
	@FXML
	void reset(ActionEvent event) {
		epidemic_sim.reset(d, s);
		clearData();
		handleDisplay();
	}
	
	@FXML 
	void playPause(ActionEvent event) {
		if (!playing) {
			playButton.setText("Pause");
			playing = true;
		} else {
			playButton.setText("Play");
			playing = false;
		}
	}
	
	@FXML 
	void step(ActionEvent event) {
		epidemic_sim.step();
		handleDisplay();
	}
	
	// Basics	
	@FXML 
	void stepSize(KeyEvent event) {
		epidemic_sim.setStep_size(Integer.parseInt(stepSizeTextField.getText()));
	}
	
	@FXML 
	void simSpeed(KeyEvent event) {
		epidemic_sim.setSim_speed(Integer.parseInt(simSpeedTextField.getText()));
	}
	
	// Disease
	@FXML
	void infectionRange(KeyEvent event) {
		Disease nD = new Disease(
				Integer.parseInt(infectionRangeTextField.getText()),
				d.getInfection_period(),
				d.getInfection_chance(),
				d.getMortality_rate()
				);
		d = nD;
		epidemic_sim.modify(d, s);
	}
	
	@FXML
	void infectiousPeriod(KeyEvent event) {
		Disease nD = new Disease(
				d.getInfection_range(),
				Integer.parseInt(infectiousPeriodTextField.getText()),
				d.getInfection_chance(),
				d.getMortality_rate()
				);
		d = nD;
		epidemic_sim.modify(d, s);
	}
	
	@FXML
	void infectionRate(MouseEvent event) {
		Disease nD = new Disease(
				d.getInfection_range(),
				d.getInfection_period(),
				infectionRateSlider.getValue(),
				d.getMortality_rate()
				);
		d = nD;
		epidemic_sim.modify(d, s);
		infectionRateDisplayLabel.setText("" + df.format(infectionRateSlider.getValue()));
	}
	
	@FXML
	void mortalityRate(MouseEvent event) {
		Disease nD = new Disease(
				d.getInfection_range(),
				d.getInfection_period(),
				d.getInfection_chance(),
				mortalityRateSlider.getValue()
				);
		d = nD;
		epidemic_sim.modify(d, s);
		mortalityRateDisplayLabel.setText("" + df.format(mortalityRateSlider.getValue()));
	}
	
	// Society
	@FXML
	void population(KeyEvent event) {
		Society nS = new Society(
				Integer.parseInt(populationTextField.getText()),
				s.getDefault_distance(),
				s.getSocial_distancing_bonus(),
				s.getSocial_distancing_percentage(),
				s.getHospital_capacity(),
				s.getHospital_mortality_percentage_reduction()
				);
		s = nS;
		epidemic_sim.modify(d, s);
		handleDisplay();
	}
	
	@FXML
	void defaultDistance(KeyEvent event) {
		Society nS = new Society(
				s.getPopulation(),
				Integer.parseInt(defaultDistanceTextField.getText()),
				s.getSocial_distancing_bonus(),
				s.getSocial_distancing_percentage(),
				s.getHospital_capacity(),
				s.getHospital_mortality_percentage_reduction()
				);
		s = nS;
		epidemic_sim.modify(d, s);
	}
	
	@FXML
	void hospitalCapacity(KeyEvent event) {
		Society nS = new Society(
				s.getPopulation(),
				s.getDefault_distance(),
				s.getSocial_distancing_bonus(),
				s.getSocial_distancing_percentage(),
				Integer.parseInt(hospitalCapacityTextField.getText()),
				s.getHospital_mortality_percentage_reduction()
				);
		s = nS;
		epidemic_sim.modify(d, s);
	}
	
	@FXML
	void mortalityReductionRate(MouseEvent event) {
		Society nS = new Society(
				s.getPopulation(),
				s.getDefault_distance(),
				s.getSocial_distancing_bonus(),
				s.getSocial_distancing_percentage(),
				s.getHospital_capacity(),
				mortalityReductionRateSlider.getValue()
				);
		s = nS;
		epidemic_sim.modify(d, s);
		mortalityReductionRateDisplayLabel.setText(df.format(mortalityReductionRateSlider.getValue()));
	}
	
	@FXML
	void bonusDistance(KeyEvent event) {
		Society nS = new Society(
				s.getPopulation(),
				s.getDefault_distance(),
				Integer.parseInt(bonusDistanceTextField.getText()),
				s.getSocial_distancing_percentage(),
				s.getHospital_capacity(),
				s.getHospital_mortality_percentage_reduction()
				);
		s = nS;
		epidemic_sim.modify(d, s);
	}
	
	@FXML
	void percentPopulation(MouseEvent event) {
		Society nS = new Society(
				s.getPopulation(),
				s.getDefault_distance(),
				s.getSocial_distancing_bonus(),
				percentPopulationSlider.getValue(),
				s.getHospital_capacity(),
				s.getHospital_mortality_percentage_reduction()
				);
		s = nS;
		epidemic_sim.modify(d, s);
		percentPopulationDisplayLabel.setText(df.format(percentPopulationSlider.getValue()));
	}
	
	// other methods
	private void clearData() {
		simGrid.getData().clear();
		sirdCurve.getData().clear();
		daysDisplayLabel.setText("n/a");
		untouchedDisplayLabel.setText("n/a");
		infectedDisplayLabel.setText("n/a");
		deadDisplayLabel.setText("n/a");
		recoveredDisplayLabel.setText("n/a");
		hidBar.getData().clear();
		pie.getData().clear();
	}
	
	public void handleDisplay() {
		
		// scatter plot
		System.out.println("scatter");
		simGrid.getData().clear();
		for (XYChart.Series s: epidemic_sim.getScatterData()) {
			System.out.println("scatter1");
			simGrid.getData().add(s);
		}
		
		// SIRD Curve
		System.out.println("curve");
		ArrayList<Integer> curveData = epidemic_sim.getAreaData();
		if (sirdCurve.getData().isEmpty()) {
			XYChart.Series untouched = new XYChart.Series<Integer, Integer>();
			untouched.setName("Untouched");
			XYChart.Series infected = new XYChart.Series<Integer, Integer>();
			infected.setName("Infected");
			XYChart.Series dead = new XYChart.Series<Integer, Integer>();
			dead.setName("Dead");
			XYChart.Series recovered = new XYChart.Series<Integer, Integer>();
			recovered.setName("Recovered");
			sirdCurve.getData().add(dead);
			sirdCurve.getData().add(infected);
			sirdCurve.getData().add(untouched);
			sirdCurve.getData().add(recovered);
		} 
		sirdCurve.getData().get(0).getData().add(new XYChart.Data(curveData.get(0), curveData.get(1)));
		sirdCurve.getData().get(1).getData().add(new XYChart.Data(curveData.get(0), curveData.get(2)));
		sirdCurve.getData().get(2).getData().add(new XYChart.Data(curveData.get(0), curveData.get(3)));
		sirdCurve.getData().get(3).getData().add(new XYChart.Data(curveData.get(0), curveData.get(4)));
		
		
		// numerical stats
		daysDisplayLabel.setText("" + curveData.get(0));
		deadDisplayLabel.setText("" + curveData.get(1));
		infectedDisplayLabel.setText("" + curveData.get(2));
		untouchedDisplayLabel.setText("" + curveData.get(3));
		recoveredDisplayLabel.setText("" + curveData.get(4));
		
		// bar
		hidBar.getData().clear();
		XYChart.Series untouched = new XYChart.Series<Integer, Integer>();
		untouched.setName("Untouched");
		XYChart.Series infected = new XYChart.Series<Integer, Integer>();
		infected.setName("Infected");
		XYChart.Series dead = new XYChart.Series<Integer, Integer>();
		dead.setName("Dead");
		XYChart.Series recovered = new XYChart.Series<Integer, Integer>();
		recovered.setName("Recovered");
		
		dead.getData().add(new XYChart.Data("Healthy", 0));
		dead.getData().add(new XYChart.Data("Infected", 0));
		dead.getData().add(new XYChart.Data("Dead", curveData.get(1)));
		hidBar.getData().add(dead);
		
		infected.getData().add(new XYChart.Data("Healthy", 0));
		infected.getData().add(new XYChart.Data("Infected", curveData.get(2)));
		infected.getData().add(new XYChart.Data("Dead", 0));
		hidBar.getData().add(infected);
		
		untouched.getData().add(new XYChart.Data("Healthy", curveData.get(3)));
		untouched.getData().add(new XYChart.Data("Infected", 0));
		untouched.getData().add(new XYChart.Data("Dead", 0));
		hidBar.getData().add(untouched);
		
		recovered.getData().add(new XYChart.Data("Healthy", curveData.get(4)));
		recovered.getData().add(new XYChart.Data("Infected", 0));
		recovered.getData().add(new XYChart.Data("Dead", 0));
		hidBar.getData().add(recovered);
		
		// pie
		pie.getData().clear();
		PieChart.Data deadSlice = new PieChart.Data("Dead", curveData.get(1));
		PieChart.Data infectedSlice = new PieChart.Data("Infected", curveData.get(2));
		PieChart.Data untouchedSlice = new PieChart.Data("Untouched", curveData.get(3));
		PieChart.Data recoveredSlice = new PieChart.Data("Recovered", curveData.get(4));
		
		pie.getData().add(deadSlice);
		pie.getData().add(infectedSlice);
		pie.getData().add(untouchedSlice);
		pie.getData().add(recoveredSlice);
	}
}
